#ifndef clipboard_win32_h_
#define clipboard_win32_h_

#include <afxstr.h>

#include <string>
#include <vector>

typedef std::pair<UINT, std::wstring> clipboard_type;

namespace clipboard {
CString from_html();
void to_html(const CString &string);
CString from_clipboard(UINT format_enum);
std::vector<clipboard_type> get_cliboard_list();
}  // namespace clipboard

#endif  // clipboard_win32_h_
