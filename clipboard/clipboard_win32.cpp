// #include <afxstr.h>
// #include <afxwin.h>
#include "stdafx.h"

#include "afxwin.h"

#include <string>
#include <vector>

#include "clipboard_win32.h"

namespace clipboard {

struct ClipboardHelper {
  static CString utf8_to_cstring(HANDLE handle) {
    CString c_str;

    DWORD handle_len = GlobalSize(handle);
    char *handle_str = (char *)GlobalLock(handle);

    char *temp_buffer = (char *)calloc(handle_len + 1, 1);
    memcpy(temp_buffer, handle_str, handle_len);
    GlobalUnlock(handle);

    temp_buffer[handle_len] = 0;

    /* Multi byte to wide char */
    int utf_len = strlen(temp_buffer);
    int wide_len = MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)temp_buffer, utf_len,
                                       nullptr, 0);
    LPWSTR unicode_buffer = (LPWSTR)calloc(wide_len + 1, sizeof(WCHAR));

    /* Stream that UTF8 to Unicode */
    MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)temp_buffer, utf_len,
                        unicode_buffer, wide_len + 1);
    c_str = unicode_buffer;

    free(temp_buffer);
    free(unicode_buffer);

    return c_str;
  }
  static CString unicode_to_cstring(HANDLE handle) {
    CString c_str;

    DWORD handle_len = GlobalSize(handle);
    wchar_t *handle_str = (wchar_t *)GlobalLock(handle);

    wchar_t *temp_buffer = (wchar_t *)calloc(handle_len + 3, 1);
    memcpy(temp_buffer, handle_str, handle_len);
    GlobalUnlock(handle);

    c_str = temp_buffer;

    free(temp_buffer);

    return c_str;
  }
};

std::vector<clipboard_type> get_cliboard_list() {
  std::vector<clipboard_type> clipboards;

  if (OpenClipboard(NULL)) {
    std::vector<UINT> formats;

    // available clipboard formats
    UINT format = 0;
    while (format = EnumClipboardFormats(format)) {
      formats.push_back(format);
    }

    LPTSTR clip_format = (LPTSTR)calloc(255, sizeof(LPTSTR));
    for (UINT format : formats) {
      memset(clip_format, 0, sizeof(LPTSTR));
      GetClipboardFormatName(format, clip_format, 255);

      clipboard_type clip;
      clip.first = format;
      clip.second = clip_format;
      clipboards.push_back(clip);
    }
  }

  CloseClipboard();

  return clipboards;
}
CString from_clipboard(UINT format_enum) {
  CString c_str;
  std::vector<clipboard_type> clipboards = get_cliboard_list();

  bool is_find = false;
  for (const clipboard_type clipboard : clipboards) {
    if (clipboard.first == format_enum) {
      is_find = true;
      break;
    }
  }
  if (is_find) {
    if (OpenClipboard(NULL)) {
      HANDLE handle = GetClipboardData(format_enum);
      c_str = ClipboardHelper::utf8_to_cstring(handle);
    }
    CloseClipboard();
  }

  return c_str;
}
CString from_html() {
  UINT CF_HTML = RegisterClipboardFormat(_T("HTML Format"));

  CString uni_clipboard_html;
  if (!IsClipboardFormatAvailable(CF_HTML)) return uni_clipboard_html;

  if (OpenClipboard(NULL)) {
    UINT format = 0;
    std::vector<UINT> formats;

    // available clipboard formats
    while (format = EnumClipboardFormats(format)) {
      formats.push_back(format);
    }

    HANDLE handle_block = GetClipboardData(CF_HTML);

    if (handle_block) {
      uni_clipboard_html = ClipboardHelper::utf8_to_cstring(handle_block);
    }
  }
  CloseClipboard();

  return uni_clipboard_html;
}
void to_html(const CString &content) {
  auto clip_header_int_to_string = [](int number) -> CString {
    WCHAR buf[100];
    _itot_s(number, buf, 10);

    int length = _tcslen(buf);
    wchar_t buffer[11] = {
        0,
    };
    swprintf_s(buffer, 11, L"%010d", number);

    CString r = buffer;
    return r;
  };
  auto make_header = [&](const CString &html) -> CString {
    CString header;
    size_t clipboard_len =
        WideCharToMultiByte(CP_UTF8, 0, html, -1, NULL, 0, NULL, NULL);
    header = "Version:0.9";      // 11
    header += "\r\nStartHTML:";  // 12
    header += clip_header_int_to_string(105);
    header += "\r\nEndHTML:";  // 10
    header += clip_header_int_to_string(105 + clipboard_len - 1);

    header += "\r\nStartFragment:";  // 16
    header += clip_header_int_to_string(105);
    header += "\r\nEndFragment:";  // 14
    header += clip_header_int_to_string(105 + clipboard_len - 1);
    header += "\r\n";  // 105

    return header;
  };

  UINT CF_HTML = RegisterClipboardFormat(_T("HTML Format"));

  if (OpenClipboard(NULL)) {
    if (EmptyClipboard()) {
      CString header = make_header(content);

      CString clipboard_data = header + content;
      size_t clipboard_len = WideCharToMultiByte(CP_UTF8, 0, clipboard_data, -1,
                                                 NULL, 0, NULL, NULL);

      HGLOBAL global_memory = GlobalAlloc(GMEM_MOVEABLE, clipboard_len);
      char *global_mem_p = (char *)GlobalLock(global_memory);
      memset(global_mem_p, 0, clipboard_len);

      WideCharToMultiByte(CP_UTF8, 0, clipboard_data,
                          clipboard_data.GetLength(), global_mem_p,
                          clipboard_len, NULL, NULL);

      GlobalUnlock(global_memory);

      SetClipboardData(CF_HTML, global_memory);
    }
  }
  CloseClipboard();
}
}  // namespace clipboard
